package javaFiles.imagePage;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ImagePage extends Application{

    Label title = null;
    public void start(Stage stage){
        
        Image ig = new Image("assets/images/C2W.jpg");
        ImageView iv = new ImageView(ig);
        iv.setFitHeight(200);
        iv.setFitWidth(300);
        iv.setPreserveRatio(true);
        title = new Label("JavaFx Practical");
        title.setFont(new Font(10));
        title.setTextFill(Color.BLUE);
        title.setAlignment(Pos.CENTER);
        title.setFont(new Font(20));
        title.setStyle("-fx-background-color:SANDYBROWN");
        title.setPrefSize(280,168);

        HBox hb = new HBox();
        hb.getChildren().addAll(iv, title);
        hb.setMaxWidth(600);
        hb.setStyle("-fx-background-color:AQUA");
        Scene sc = new Scene(hb , 1200, 600, Color.AQUA);
        stage.setScene(sc);
        stage.show();
    }
    
}
