package javaFiles.simpleWindow;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Window1 extends Application{
    public void start(Stage stage){
        stage.setTitle("C2W_Assignment_1");
        stage.setHeight(450);
        stage.setWidth(600);
        
        Text text = new Text("Super - X 2024 By Core2web");
        text.setLayoutX(100);
        text.setLayoutY(200);
        text.setFont(new Font(30));
        
        Group gr = new Group(text);
        Scene scene = new Scene(gr);
        scene.setFill(Color.ORANGE);
        stage.setScene(scene);
        stage.show();
    }
}
