package javaFiles.simpleWindow;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Window2 extends Application{
   
    Text name = null;
    Text hobbies = null;
    Text pLang = null;
    
    public void start(Stage stage){
        
        name = new Text("Ruchira Suresh Malwade");
        name.setFont(new Font(30));
        name.setFill(Color.MAROON);
        name.setLayoutX(380);
        name.setLayoutY(60);
        
        hobbies = new Text(30,30,"Hobbies");
        hobbies.setFont(new Font(20));
        hobbies.setFill(Color.DARKBLUE);
        Text yoga = new Text("Yoga");
        yoga.setFont(new Font(20));
        Text coding = new Text("Coding");
        coding.setFont(new Font(20));
        Text badminton = new Text("Playing Badminton");
        badminton.setFont(new Font(20));

        pLang = new Text("Programming Languages I Like");
        pLang.setFont(new Font(20));
        pLang.setFill(Color.DARKBLUE);
        Text java = new Text("Java");
        java.setFont(new Font(20));
        Text javascript = new Text("Javascript");
        javascript.setFont(new Font(20));
        Text python = new Text("Python");
        python.setFont(new Font(20));

        VBox vb1 = new VBox(20, hobbies, yoga, coding, badminton);
        VBox vb2 = new VBox(20, pLang, java, javascript, python);
        vb1.setStyle("-fx-background-color:BISQUE");
        vb2.setStyle("-fx-background-color:BISQUE");
        vb1.setPrefSize(300, 300);
        vb2.setPrefSize(300,300);
        vb1.setAlignment(Pos.BASELINE_CENTER);
        vb2.setAlignment(Pos.BASELINE_CENTER);

        HBox hb = new HBox(10, vb1, vb2);
        hb.setLayoutX(240);
        hb.setLayoutY(100);

        Group gr = new Group(name, hb);
        Scene scene = new Scene(gr);
        scene.setFill(Color.SANDYBROWN);

        stage.setScene(scene);
        stage.setTitle("MyWindow");
        stage.setHeight(600);
        stage.setWidth(1100);
        stage.show();
    }
}
