package javaFiles.buttonWindow;
import java.util.function.UnaryOperator;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ButtonWindow extends Application{
    // Assignment 1
    Text text = null;
    // Assignment 2
    int val = 0;
    String square = null;
    // Assignment 7
    int i = 0;
    public void start(Stage primaryStage){

        primaryStage.setTitle("ButtonWindow");

        // Assignment 1

        Button jan = new Button("January");
        jan.setPrefWidth(150);
        jan.setFont(new Font(15));
        jan.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button feb = new Button("February");
        feb.setPrefWidth(150);
        feb.setFont(new Font(15));
        feb.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button march = new Button("March");
        march.setPrefWidth(150);
        march.setFont(new Font(15));
        march.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button april = new Button("April");
        april.setPrefWidth(150);
        april.setFont(new Font(15));
        april.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button may = new Button("May");
        may.setPrefWidth(150);
        may.setFont(new Font(15));
        may.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button june = new Button("June");
        june.setPrefWidth(150);
        june.setFont(new Font(15));
        june.setStyle("-fx-background-color:DEEPSKYBLUE");

        Button july = new Button("July");
        july.setPrefWidth(150);
        july.setFont(new Font(15));
        july.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button august = new Button("August");
        august.setPrefWidth(150);
        august.setFont(new Font(15));
        august.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button sep = new Button("September");
        sep.setPrefWidth(150);
        sep.setFont(new Font(15));
        sep.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button oct = new Button("Octobor");
        oct.setPrefWidth(150);
        oct.setFont(new Font(15));
        oct.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button nov = new Button("November");
        nov.setPrefWidth(150);
        nov.setFont(new Font(15));
        nov.setStyle("-fx-background-color:DEEPSKYBLUE");
        Button dec = new Button("December");
        dec.setPrefWidth(150);
        dec.setFont(new Font(15));
        dec.setStyle("-fx-background-color:DEEPSKYBLUE");

        jan.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("January");
            }
        });
        feb.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("February");
            }
        });
        march.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("March");
            }
        });
        april.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("April");
            }
        });
        may.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("May");
            }
        });
        june.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("June");
            }
        });
        july.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("July");
            }
        });
        august.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("August");
            }
        });
        sep.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("September");
            }
        });
        oct.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("October");
            }
        });
        nov.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("November");
            }
        });
        dec.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                System.out.println("December");
            }
        });

        VBox vb1 = new VBox(20, jan, feb, march, april, may, june);
        VBox vb2 = new VBox(20, july, august, sep, oct, nov, dec);
        HBox hb1 = new HBox(20, vb1, vb2);
        hb1.setStyle("-fx-background-color:GOLDEN");
        hb1.setAlignment(Pos.TOP_LEFT);
        hb1.setLayoutX(10);
        hb1.setLayoutY(10);

        // Assignment 2

        TextField txt = new TextField();
        txt.setPrefWidth(300);
        txt.setFont(new Font(15));
        Button print = new Button("Print Text"); 
        print.setStyle("-fx-background-color:DEEPSKYBLUE");
        print.setFont(new Font(15));

        VBox vbAssign2 = new VBox(20, txt, print);
        vbAssign2.setAlignment(Pos.CENTER);

        print.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                System.out.println(txt.getText());
                text = new Text(txt.getText());
                text.setFont(new Font(20));
                vbAssign2.getChildren().addAll(text);
            }
        });
        HBox hb2 = new HBox(vbAssign2);
        hb2.setStyle("-fx-background-color:AQUA");
        hb2.setAlignment(Pos.CENTER);

        // Assignment 3

        TextField num = new TextField();
        num.setPromptText("Enter numbers only");
        UnaryOperator<TextFormatter.Change> filter = change -> {
            String text = change.getText();
            if(text.matches("[0-9]*")){
                return change;
            }
            return null;
        };
        TextFormatter<String> tf = new TextFormatter<>(filter);
        num.setTextFormatter(tf);

        Button sqr = new Button("Square");
        Button sqrRoot = new Button("Square Root");

        VBox vbAssign3 = new VBox(20, num, sqr, sqrRoot);
        vbAssign3.setAlignment(Pos.CENTER);
        sqr.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                String str = num.getText();
                val = Integer.parseInt(str);
                val = val*val;
                System.out.println("Square of "+ str +" is : " + val);

                String strDisplay = Integer.toString(val);
                Text s = new Text("Square of "+ str +" is : " + strDisplay);
                vbAssign3.getChildren().add(s);
                sqr.setDisable(true);
            }
        }); 
        sqrRoot.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                String str = num.getText();
                val = Integer.parseInt(str);
                val = (int) (Math.sqrt(val));
                System.out.println("Square Root of "+ str +" is : "+ val);
                String strDisplay = Integer.toString(val);
                Text s = new Text("Square Root of "+ str +" is : "+ strDisplay);
                vbAssign3.getChildren().add(s);
                sqrRoot.setDisable(true);
            }
        });
        HBox hb3 = new HBox(vbAssign3);
        hb3.setAlignment(Pos.CENTER);
        hb3.setStyle("-fx-background-color:AQUA");

        // Assignment 4

        PasswordField pass = new PasswordField();
        Button show = new Button("Print Password");
        show.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                System.out.println(pass.getText());
            }
        });
        VBox vbAssign4 = new VBox(20, pass, show);
        vbAssign4.setAlignment(Pos.CENTER);
        HBox hb4 = new HBox(vbAssign4);
        hb4.setStyle("-fx-background-color:AQUA");
        hb4.setAlignment(Pos.CENTER);

        // Assignment 5

        TextField name = new TextField();
        PasswordField password = new PasswordField();
        Button login = new Button("Login");
        VBox vbAssign5 = new VBox(20, name, password, login);
        vbAssign5.setAlignment(Pos.CENTER);

        login.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                String userName = "Ruchira";
                String userPass = "Ruchira@123";
                String str1 = name.getText();
                String str2 = password.getText();
                
                if(str1.equals(userName) && str2.equals(userPass)){
                    Text txt = new Text("You are Welcome!!");
                    txt.setFont(new Font(20));
                    vbAssign5.getChildren().addAll(txt);
                    login.setDisable(true);
                }else{
                    Text alert = new Text("Please try again!");
                    alert.setFont(new Font(20));
                    vbAssign5.getChildren().addAll(alert);
                    login.setDisable(true);
                }
            }
        });
        HBox hb5 = new HBox(vbAssign5);
        hb5.setAlignment(Pos.CENTER);
        hb5.setStyle("-fx-background-color:AQUA");

        // Assignment 6

        TextField text = new TextField();
        Button check = new Button("Check");
        VBox vbAssign6 = new VBox(20, text, check);
        vbAssign6.setAlignment(Pos.CENTER);

        check.setOnAction(new EventHandler<ActionEvent>() {
            public boolean palindrome(String str){
                boolean res = false;
                String rev = "";
                for(int i=str.length()-1; i>=0; i--){
                    rev = rev + str.charAt(i);
                }
                if(str.equals(rev)){
                    res = true;
                }
                return res;
            }
            public void handle(ActionEvent event){
                String str = text.getText();
                if(palindrome(str) == true){
                    Text res = new Text("Palindrome");
                    res.setFont(new Font(20));
                    vbAssign6.getChildren().addAll(res);
                }else{
                    Text res = new Text("Not Palindrome");
                    res.setFont(new Font(20));
                    vbAssign6.getChildren().addAll(res);
                }
            }
        });

        HBox hb6 = new HBox(vbAssign6);
        hb6.setAlignment(Pos.CENTER);
        hb6.setStyle("-fx-background-color:AQUA");

        // Assignment 7

        Button click = new Button("Click Me!");
        click.setLayoutX(360);
        click.setLayoutY(280);
        click.setStyle("-fx-background-color:BLUEVIOLET");
        Group gr = new Group(click);
        Scene sc = new Scene(hb1, 800, 600, Color.AQUA);
            click.setOnAction(new EventHandler<ActionEvent>() {

                public void handle(ActionEvent event){ 
                        if(i==0){
                            sc.setFill(Color.BLUEVIOLET);
                            click.setStyle("-fx-background-color:GREENYELLOW");
                            i++;
                        }else if(i==1){
                            sc.setFill(Color.GREENYELLOW);
                            click.setStyle("-fx-background-color:AQUA");
                            i++;
                        }else if(i==2){
                            sc.setFill(Color.AQUA);
                            click.setStyle("-fx-background-color:YELLOW");
                            i++;
                        }else{
                            sc.setFill(Color.YELLOW);
                            click.setStyle("-fx-background-color:BLUEVIOLET");
                            i=0;
                        }
                }
            });
        primaryStage.setScene(sc);
        primaryStage.show();
    }
}
