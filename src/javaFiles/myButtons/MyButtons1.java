package javaFiles.myButtons;

import javafx.stage.Stage;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class MyButtons1 extends Application{

    Label name = null;
    Label pass = null;
    public void start(Stage stage){
        stage.setTitle("MyButtons");
        Label username = new Label("Username");
        username.setFont(new Font(15));
        TextField txt = new TextField();
        txt.setMaxWidth(300);
        txt.setFont(new Font(15));
        Label password = new Label("Password");
        password.setFont(new Font(15));
        PasswordField ps = new PasswordField();
        ps.setMaxWidth(300);
        ps.setFont(new Font(15));
        Button show = new Button("Show");
        show.setFont(new Font(15));

        HBox hb1 = new HBox(10, username, txt);
        hb1.setAlignment(Pos.CENTER);
        HBox hb2 = new HBox(10, password, ps);
        hb2.setAlignment(Pos.CENTER);
        VBox vb = new VBox(20, hb1, hb2, show);
        vb.setCursor(Cursor.OPEN_HAND);
        vb.setStyle("-fx-background-color:AQUA");
        vb.setAlignment(Pos.CENTER);

        show.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent event){
                Label n = new Label("Username : ");
                n.setFont(new Font(15));
                name = new Label(txt.getText());
                name.setFont(new Font(20));
                Label p = new Label("Password : ");
                p.setFont(new Font(15));
                pass = new Label(ps.getText());
                pass.setFont(new Font(20));
                HBox hb = new HBox(20, n, name, p, pass);
                hb.setAlignment(Pos.CENTER);
                vb.getChildren().addAll(hb);
            }
            
        });

        vb.setMaxSize(200, 200);
        Scene scene = new Scene(vb);
        stage.setScene(scene);
        stage.setHeight(600);
        stage.setWidth(800);
        stage.show();
    }
}
