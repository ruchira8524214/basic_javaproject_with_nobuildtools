package javaFiles.myButtons;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MyButtons2 extends Application{
    public void start(Stage primaryStage){
        primaryStage.setTitle("ButtonsAssignment");

        // Assignment 1
        Button hello = new Button("Hello Super - X");
        VBox vb1 = new VBox(hello);
        vb1.setAlignment(Pos.CENTER);

        hello.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                System.out.println("Core2web!");
            }
        });

        // Assignment 2

        Label c2w = new Label("Core2web.in");
        c2w.setTextFill(Color.BLACK);
        Button c2wJava = new Button("Core2web - Java");
        c2wJava.setTextFill(Color.BLACK);
        c2wJava.setStyle("-fx-background-color:MEDIUMBLUE");
        Button c2wSuperX = new Button("Core2web - Super-X");
        c2wSuperX.setTextFill(Color.BLACK);
        c2wSuperX.setStyle("-fx-background-color:MEDIUMBLUE");
        Button c2wDSA = new Button("Core2web - DSA");
        c2wDSA.setTextFill(Color.BLACK);
        c2wDSA.setStyle("-fx-background-color:MEDIUMBLUE");

        VBox vb2 = new VBox(20, c2w, c2wJava, c2wSuperX, c2wDSA);
        vb2.setStyle("-fx-background-color:GOLD");
        vb2.setAlignment(Pos.CENTER);

        c2wJava.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                c2wJava.setStyle("-fx-background-color:GREEN");
            }
        });
        c2wSuperX.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                c2wSuperX.setStyle("-fx-background-color:GREEN");
            }
        });
        c2wDSA.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                c2wDSA.setStyle("-fx-background-color:GREEN");
            }
        });

        // Assignment 3
        
        Button os = new Button("OS");
        os.setStyle("-fx-background-color:MEDIUMBLUE");
        os.setTextFill(Color.BLACK);
        Button c = new Button("C");
        c.setStyle("-fx-background-color:MEDIUMBLUE");
        c.setTextFill(Color.BLACK);
        Button cpp = new Button("CPP");
        cpp.setStyle("-fx-background-color:MEDIUMBLUE");
        cpp.setTextFill(Color.BLACK);
        Button java = new Button("Java");
        java.setStyle("-fx-background-color:MEDIUMBLUE");
        java.setTextFill(Color.BLACK);
        Button dsa = new Button("DSA");
        dsa.setStyle("-fx-background-color:MEDIUMBLUE");
        dsa.setTextFill(Color.BLACK);
        Button python = new Button("Python");
        python.setStyle("-fx-background-color:MEDIUMBLUE");
        python.setTextFill(Color.BLACK);
        
        HBox hb1 = new HBox(10, os, c, cpp);
        hb1.setAlignment(Pos.BOTTOM_CENTER);
        HBox hb2 = new HBox(10, java, dsa, python);
        hb2.setAlignment(Pos.BOTTOM_CENTER);
        VBox vb3 = new VBox(20, c2w, hb1, hb2);
        vb3.setAlignment(Pos.CENTER);
        vb3.setStyle("-fx-background-color:GOLD");
        Scene sc = new Scene(vb3, 600, 400);
        primaryStage.setScene(sc);
        primaryStage.show();
    }
}
