package javaFiles.dialogBox;

import java.util.Optional;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DialogBox extends Application{
    public void start(Stage primaryStage){

        primaryStage.setTitle("Dialog Box Demo");
        TextField txtField = new TextField();
        Dialog<String>dialog = new Dialog<String>();
        dialog.setTitle("C2W_Dialog");
        ButtonType bType = new ButtonType("OK", ButtonData.OK_DONE);
        dialog.setContentText("Dialog box created successfully");
        dialog.getDialogPane().getButtonTypes().add(bType);
        Text txt = new Text("Click here --->");

        Font font = Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12);
        txt.setFont(font);

        Button button = new Button("Show");

        button.setOnAction(e->extracted(txtField, dialog));
        HBox pane = new HBox(15);
        pane.setPadding(new Insets(50,150,50,60));
        pane.getChildren().addAll(txt, button);

        Alert info = new Alert(AlertType.INFORMATION);
        info.setTitle("Information Dialog");
        info.setHeaderText(null);
        info.setContentText("I have a great message for you!");
        info.showAndWait();

        Alert warn = new Alert(AlertType.WARNING);
        warn.setTitle("Warning Dialog");
        warn.setHeaderText("Look, a Warning Dialog");
        warn.setContentText("Careful with your next step!");
        warn.showAndWait();

        Alert err = new Alert(AlertType.ERROR);
        err.setTitle("Error Dialog");
        err.setHeaderText("Look, an Error Dialog");
        err.setContentText("Oops, there was an error!");
        err.showAndWait();

        Alert confirm = new Alert(AlertType.CONFIRMATION);
        confirm.setTitle("Confirmation Dialog");
        confirm.setHeaderText("Look, a Confirmation Dialog");
        confirm.setContentText("Are you ok with this?");
        confirm.showAndWait();

        Alert confirm2 = new Alert(AlertType.CONFIRMATION);
        confirm2.setTitle("Confirmation Dialog");
        confirm2.setHeaderText("Look, a Confirmation Dialog with Custom Actions");
        confirm2.setContentText("Choose your option");
        ButtonType one = new ButtonType("One");
        ButtonType two = new ButtonType("Two");
        ButtonType three = new ButtonType("Three");
        ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        confirm2.getButtonTypes().setAll(one, two, three, cancel);
        Optional<ButtonType> result = confirm2.showAndWait();

        // CheckBox

        Label checkBoxLabel = new Label("This is check box label!");
        String[] checkBoxes = {"Core2web", "Incubators", "Biencaps"};
         
        VBox checkBoxVb = new VBox(20, checkBoxLabel);

        for (String checkBox : checkBoxes){

            CheckBox cb = new CheckBox(checkBox);
            checkBoxVb.getChildren().add(cb);
        }

        Scene scCheckBox = new Scene(checkBoxVb, 200, 200);

        // RadioButtons

        VBox vbRadio = new VBox(10);
        Label label = new Label("Career Option: ");
        RadioButton rb1 = new RadioButton("Backend Developer");
        RadioButton rb2 = new RadioButton("Web Developer");
        RadioButton rb3 = new RadioButton("Flutter Developer");

        ToggleGroup group = new ToggleGroup();
        rb1.setToggleGroup(group);
        rb2.setToggleGroup(group);
        rb3.setToggleGroup(group);

        vbRadio.getChildren().addAll(label, rb1, rb2, rb3);

        Scene scRadio = new Scene(vbRadio, 200, 200);

        Scene sc = new Scene(new Group(pane), 600, 250, Color.TEAL);
        primaryStage.setScene(sc);
        primaryStage.show();
    }
    
    private void extracted(TextField textField, Dialog<String> dialog){
        dialog.showAndWait();
    }
}
