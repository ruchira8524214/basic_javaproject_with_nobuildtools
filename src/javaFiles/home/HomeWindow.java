package javaFiles.home;

import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HomeWindow extends Application{
    Text title = null;
    Text text = null;
    Text java = null;
    Text python = null;
    Text cpp = null;
    Text html = null;
    Text css = null;
    Text javascript = null;
    
    public void start(Stage projectStage){
        projectStage.setTitle("MYFirstProject");
        projectStage.setWidth(1000);
        projectStage.setHeight(500);
        projectStage.setResizable(false);
        projectStage.getIcons().add(new Image("assets/images/logo.png"));

        title = new Text(20,30,"Programming Languages");
        title.setFont(new Font(20));
        title.setFill(Color.LIGHTBLUE);
        text = new Text(80, 400, "Have a Nice Day!");
        text.setFill(Color.IVORY);
        text.setFont(new Font(30));
       
        java = new Text(10,40,"Java");
        java.setFont(new Font(15));
        java.setFill(Color.BISQUE);
        python = new Text(10,60,"Python");
        python.setFill(Color.BISQUE);
        python.setFont(new Font(15));
        cpp = new Text(10,80,"CPP");
        cpp.setFill(Color.BISQUE);
        cpp.setFont(new Font(15));

        html = new Text(10,20,"HTML");
        html.setFill(Color.ORANGE);
        html.setFont(new Font(15));
        css = new Text(10, 60, "CSS");
        css.setFill(Color.ORANGE);
        css.setFont(new Font(15));
        javascript = new Text(10,80,"Javascript");
        javascript.setFill(Color.ORANGE);
        javascript.setFont(new Font(15));

        VBox vb1 = new VBox(20, java, python, cpp);
        vb1.setLayoutX(300);
        vb1.setLayoutY(300);

        VBox vb2 = new VBox(20, html, css, javascript);
        vb2.setLayoutX(300);
        vb2.setLayoutY(300);
        // hb.getChildren().addAll(html,css,javascript);

        HBox hb = new HBox(30, vb1,vb2);
        hb.setLayoutX(50);
        hb.setLayoutY(80);

        Group gr = new Group(hb, title, text);
        Scene sc = new Scene(gr);
        sc.setFill(Color.DARKVIOLET);
        sc.setCursor(Cursor.OPEN_HAND);
        projectStage.setScene(sc);
        projectStage.show();
    }
}
