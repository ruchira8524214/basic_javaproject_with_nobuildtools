package javaFiles.home;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MyPane extends Application{
    Label label = null;
    
    public void start(Stage paneStage){
        paneStage.setTitle("MyPane");
        label = new Label("Good Morning!");
        label.setFont(new Font(30));
        label.setAlignment(Pos.CENTER);;
        label.setLayoutX(380);
        label.setLayoutY(150);
        label.setTextFill(Color.BLUE);

        Label topLeft = new Label("Top Left");
        topLeft.setFont(new Font(20));
        topLeft.setLayoutX(10);
        topLeft.setLayoutY(10);
        Label topRight = new Label("Top Right");
        topRight.setFont(new Font(20));
        topRight.setLayoutX(860);
        topRight.setLayoutY(10);
        Label bottomLeft = new Label("Bottom Left");
        bottomLeft.setFont(new Font(20));
        bottomLeft.setLayoutX(10);
        bottomLeft.setLayoutY(420);
        Label bottomRight = new Label("Bottom Right");
        bottomRight.setFont(new Font(20));
        bottomRight.setLayoutX(860);
        bottomRight.setLayoutY(420);

        Pane pane = new Pane(label,topLeft,topRight,bottomLeft,bottomRight);
        BackgroundFill bg = new BackgroundFill( Color.valueOf("#ff00"),
                            new CornerRadii(5),
                            new Insets(5));
        Background bG = new Background(bg);
        pane.setBackground(bG);
        
        Scene sc = new Scene(pane);
        sc.setFill(Color.DARKORANGE);
        sc.setCursor(Cursor.OPEN_HAND);
        
        paneStage.setScene(sc);
        paneStage.setHeight(500);
        paneStage.setWidth(1000);
        paneStage.setResizable(false);
        paneStage.show();
    }
}
